# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.pool import PoolMeta
from trytond.model import fields

        
class LiquidationLineAdjustment(metaclass=PoolMeta):
    __name__ = 'staff.liquidation.line_adjustment'
    
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account')
    

class LiquidationAdjustmentStart(metaclass=PoolMeta):
    __name__ = 'staff.liquidation_adjustment.start'
    
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account')


class LiquidationAdjustment(metaclass=PoolMeta):
    __name__ = 'staff.liquidation_adjustment'

    def create_adjustment(self, line):
        adjust = super(LiquidationAdjustment, self).create_adjustment(line)
        adjust.write([adjust], {'analytic_account': self.start.analytic_account.id})
        return adjust
