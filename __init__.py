# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import payroll
from . import employee
from . import liquidation
from . import account


def register():
    Pool.register(
        employee.MandatoryWage,
        employee.AddAnalyticStart,
        payroll.Payroll,
        payroll.PayrollLine,
        account.AnalyticAccountEntry,
        liquidation.LiquidationLineAdjustment,
        liquidation.LiquidationAdjustmentStart,
        module='analytic_payroll', type_='model')
    Pool.register(
        employee.AddAnalytic,
        payroll.PayrollAddAnalytic,
        liquidation.LiquidationAdjustment,
        payroll.PayrollMoveAnalytic,
        module='analytic_payroll', type_='wizard')
