# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.wizard import (
    Wizard, StateView, StateTransition, Button
)


class MandatoryWage(metaclass=PoolMeta):
    __name__ = 'staff.payroll.mandatory_wage'
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1))
        ])

    @classmethod
    def __setup__(cls):
        super(MandatoryWage, cls).__setup__()


class AddAnalyticStart(ModelView):
    'Add Analytic Start'
    __name__ = 'analytic_payroll.employee_add_analytic.start'
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1))
        ])


class AddAnalytic(Wizard):
    'Add Analytic'
    __name__ = 'analytic_payroll.employee_add_analytic'
    start = StateView('analytic_payroll.employee_add_analytic.start',
        'analytic_payroll.employee_add_analytic_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Add', 'add', 'tryton-ok', default=True),
        ])
    add = StateTransition()

    def transition_add(self):
        pool = Pool()
        Employee = pool.get('company.employee')
        Mandatory = pool.get('staff.payroll.mandatory_wage')

        ids = Transaction().context['active_ids']
        if not ids:
            return 'end'
        employees = Employee.browse(ids)

        to_add = []
        for e in employees:
            for mw in e.mandatory_wages:
                to_add.append(mw)
        if not to_add:
            return 'end'

        Mandatory.write(
            to_add, {'analytic_account': self.start.analytic_account}
        )

        return 'end'
